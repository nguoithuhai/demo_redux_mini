import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_Demo = combineReducers({
  soLuong: numberReducer,
  //soluong là key : quản lý reducer con
  // value là tên reducer con(numberReducer) đặt
});
