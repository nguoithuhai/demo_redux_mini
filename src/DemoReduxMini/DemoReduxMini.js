import React, { Component } from "react";
import { connect } from "react-redux";

class DemoReduxMini extends Component {
  render() {
    console.log("prop", this.props);
    return (
      <div>
        <button onClick={this.props.handleGiam} className="btn btn-danger mx-2">
          -
        </button>
        <strong className="mx-2">{this.props.currentNumber}</strong>
        <button
          onClick={this.props.handleTang}
          className="btn btn-success mx-2"
        >
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    currentNumber: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "Tang_So_Luong",
      };
      dispatch(action);
    },
    handleGiam: () => {
      let action = {
        type: "Giam_So_Luong",
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
